# Keyboard Layouts

Alternative keyboard layouts for US keyboards.

# OS/languages supported

- macOS Icelandic
- macOS Swedish

# Installation

macOS: Copy the `.bundle` directories into `~/Library/Keyboard Layouts/`.